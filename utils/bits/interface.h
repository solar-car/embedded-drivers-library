/**
 * Bit Manipulation API
 *
 * Authors: Michael Rouse
 */
#ifndef __BIT_MANIPULATION_INTERFACE__
#define __BIT_MANIPULATION_INTERFACE__

/* Sets bits high */
#define bsh(...)                bits_set_high(__VA_ARGS__)
#define bits_set_high(...)      CAT(__bsh_, NUM_ARGS(__VA_ARGS__))(__VA_ARGS__)

#define __bsh_0()                                               ();
#define __bsh_1(v)                                              ();
#define __bsh_2(v, b1)                                          (v |= b1) 
#define __bsh_3(v, b1, b2)                                      (v |= b1 + b2)
#define __bsh_4(v, b1, b2, b3)                                  (v |= b1 + b2 + b3)
#define __bsh_5(v, b1, b2, b3, b4)                              (v |= b1 + b2 + b3 + b4)
#define __bsh_6(v, b1, b2, b3, b4, b5)                          (v |= b1 + b2 + b3 + b4 + b5)
#define __bsh_7(v, b1, b2, b3, b4, b5, b6)                      __bsh_4(v, b1, b2, b3); __bsh_4(v, b4, b5, b6);
#define __bsh_8(v, b1, b2, b3, b4, b5, b6, b7)                  __bsh_7(v, b1, b2, b3, b4, b5, b6); __bsh_2(v, b7);
#define __bsh_9(v, b1, b2, b3, b4, b5, b6, b7, b8)              __bsh_4(v, b1, b2, b3); __bsh_4(v, b4, b5, b6); __bsh_3(v, b7, b8);
#define __bsh_10(v, b1, b2, b3, b4, b5, b6, b7, b8, b9)         __bsh_4(v, b1, b2, b3); __bsh_4(v, b4, b5, b6); __bsh_4(v, b7, b8, b9);
#define __bsh_11(v, b1, b2, b3, b4, b5, b6, b7, b8, b9, b10)    __bsh_4(v, b1, b2, b3); __bsh_4(v, b4, b5, b6); __bsh_4(v, b7, b8, b9); __bsh_2(v, b10);

/* Sets bits low */
#define bsl(...)                bits_set_low(__VA_ARGS__)
#define bits_set_low(...)       (CAT(__bsl_, NUM_ARGS(__VA_ARGS__))(__VA_ARGS__))

#define __bsl_0()                                               ()
#define __bsl_1(v)                                              ()
#define __bsl_2(v, b1)                                          (v &= ~b1) 
#define __bsl_3(v, b1, b2)                                      (v &= ~(b1 + b2))
#define __bsl_4(v, b1, b2, b3)                                  (v &= ~(b1 + b2 + b3))
#define __bsl_5(v, b1, b2, b3, b4)                              (v &= ~(b1 + b2 + b3 + b4))
#define __bsl_6(v, b1, b2, b3, b4, b5)                          (v &= ~(b1 + b2 + b3 + b4 + b5))
#define __bsl_7(v, b1, b2, b3, b4, b5, b6)                      __bsl_4(v, b1, b2, b3); __bsl_4(v, b4, b5, b6);
#define __bsl_8(v, b1, b2, b3, b4, b5, b6, b7)                  __bsl_7(v, b1, b2, b3, b4, b5, b6); __bsl_2(v, b7);
#define __bsl_9(v, b1, b2, b3, b4, b5, b6, b7, b8)              __bsl_4(v, b1, b2, b3); __bsl_4(v, b4, b5, b6); __bsl_3(v, b7, b8);
#define __bsl_10(v, b1, b2, b3, b4, b5, b6, b7, b8, b9)         __bsl_4(v, b1, b2, b3); __bsl_4(v, b4, b5, b6); __bsl_4(v, b7, b8, b9);
#define __bsl_11(v, b1, b2, b3, b4, b5, b6, b7, b8, b9, b10)    __bsl_4(v, b1, b2, b3); __bsl_4(v, b4, b5, b6); __bsl_4(v, b7, b8, b9); __bsl_2(v, b10);

/* Toggles a bit */
//#define bits_toggle(value, bits)    ((value) ^= (bits))
#define bt(...)                 bits_toggle(__VA_ARGS__)
#define bits_toggle(...)        CAT(__bt_, NUM_ARGS(__VA_ARGS__))(__VA_ARGS__)

#define __bt_0()                                               ();
#define __bt_1(v)                                              ();
#define __bt_2(v, b1)                                          (v ^= b1)
#define __bt_3(v, b1, b2)                                      __bt_2(v, b1); __bt_2(v, b2);
#define __bt_4(v, b1, b2, b3)                                  __bt_3(v, b1, b2); __bt_2(v, b3);
#define __bt_5(v, b1, b2, b3, b4)                              __bt_4(v, b1, b2, b3); __bt_2(v, b4);
#define __bt_6(v, b1, b2, b3, b4, b5)                          __bt_5(v, b1, b2, b3, b4); __bt_2(v, b5);
#define __bt_7(v, b1, b2, b3, b4, b5, b6)                      __bt_4(v, b1, b2, b3); __bt_4(v, b4, b5, b6);
#define __bt_8(v, b1, b2, b3, b4, b5, b6, b7)                  __bt_7(v, b1, b2, b3, b4, b5, b6); __bt_2(v, b7);
#define __bt_9(v, b1, b2, b3, b4, b5, b6, b7, b8)              __bt_4(v, b1, b2, b3); __bt_4(v, b4, b5, b6); __bt_3(v, b7, b8);
#define __bt_10(v, b1, b2, b3, b4, b5, b6, b7, b8, b9)         __bt_4(v, b1, b2, b3); __bt_4(v, b4, b5, b6); __bt_4(v, b7, b8, b9);
#define __bt_11(v, b1, b2, b3, b4, b5, b6, b7, b8, b9, b10)    __bt_4(v, b1, b2, b3); __bt_4(v, b4, b5, b6); __bt_4(v, b7, b8, b9); __bt_2(v, b10);

/* Gets a bit */
#define bits_get(value, bits)       ((value) & (bits))

/* Checks if a bit is High or Low */
#define bits_is_high(value, bits)   (bits_get(value, bits) != 0x00)
#define bits_is_low(value, bits)    (bits_get(value, bits) == 0x00)

/* Bit shifting */
#define bits_left_shift(value, n)   ((value) << (n))
#define bits_right_shift(value, n)  ((value) >> (n))

#endif